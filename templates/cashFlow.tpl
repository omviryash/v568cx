<A href="index.php">Home</A>&nbsp;&nbsp;
<A href="mnuAccount.php">Menu</A>
<BODY>
<FORM name=Form1 action="{$PHP_SELF}" method=POST>
<TABLE border="0" cellPadding="2" cellSpacing="0">
<TR>
 	<TD>&nbsp;&nbsp;Transaction In&nbsp;</TD>
 	<TD>
 		<select name="transModeOpt" onChange="document.Form1.submit();">
	    <OPTION value="0">All </OPTION>
	    {html_options values=$bank.id output=$bank.name selected=$selectBankId}
    </select>
  </Td>
</TR>
</TABLE>
</FORM>
  <TABLE BORDER=1 cellspacing="0" cellpadding="3">
  <TR>
    <TH colspan="7">&nbsp;</TH>
  </TR>
  <TR>
    <TH ALIGN="center">Delete</TH>
    <TH ALIGN="center">Date</TH>
    <TH align="center">Transaction Type</TH>
    <TH ALIGN="center">Note</TH>
    <TH ALIGN="center">
      ! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !
    </TH>
    <TH ALIGN="center" NOWRAP>
      ! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !
    </TH>
    <TH>Total</TH>
  </TR>
    {section name=sec2 loop=$cashFlowCount}
      <TR>
        <TD>
          <A onclick="return confirm('Are you sure?');"
          href="accCashFlowDelete.php?cashFlowId={$cashFlow[sec2].id}&amp;page=cashFlow">Delete</A>
        </TD>
        <TD NOWRAP>{$cashFlow[sec2].date}</TD>
        <TD NOWRAP>{$cashFlow[sec2].transtype}</TD>
        <TD>&nbsp; {$cashFlow[sec2].notes}</TD>
        {if $cashFlow[sec2].dwstatus == 'w'}  
          <TD NOWRAP align="right">&nbsp;{$cashFlow[sec2].amount}</TD>
          <TD NOWRAP align="right">&nbsp;</TD>
        {else}
          <TD NOWRAP align="right">&nbsp;</TD>
          <TD NOWRAP align="right">&nbsp;{$cashFlow[sec2].amount}</TD>
        {/if}
        <TD align="right"><B>&nbsp;{math equation="abs(x)" x=$cashFlow[sec2].totalAmount format="%.2f"}</B></TD>
      </TR>
    {/section}
    {section name=sec3 loop=$otherExpCount}
      <TR>
        <TD>
            <A onclick="return confirm('Are you sure?');"
            href="accCashFlowDelete.php?expId={$otherExp[sec3].id}&amp;page=cashFlow">Delete</A>
        </TD>
        <TD NOWRAP>{$otherExp[sec3].date}</TD>
        <TD NOWRAP>Expences : {$otherExp[sec3].name}</TD>
        <TD>&nbsp;{$otherExp[sec3].note}</TD>
          <TD NOWRAP align="right">&nbsp;</TD>
          <TD NOWRAP align="right">&nbsp;{$otherExp[sec3].amount}</TD>
        <TD align="right"><B>&nbsp;{math equation="abs(x)" x=$otherExp[sec3].totalAmount format="%.2f"}</B></TD>
      </TR>
    {/section}

  </TABLE>
</BODY>