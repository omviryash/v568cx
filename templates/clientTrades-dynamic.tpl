<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
.lossStyle {
	color: red
}
.profitStyle {
	color: blue
}
</STYLE>  

<script src="./js/jquery.min.js"></script>
<script type="text/javascript">


jQuery(document).ready(function($){

  $("#storeLocally").click(function() {
    // alert("Hello");
    var calculations = [];
    $(".price").each(function(i, ele) {
      calculations.push({
        'item': $(ele).data('item'),
        'expiry': $(ele).data('expiry'),
        'value': $(ele).val()
      });
    }).promise().done(function() {
      window.sessionStorage.setItem('calculations', JSON.stringify(calculations));
    });
  });


  $(".price").change(function(){
    var self = this;
    var item = $(self).data('item');
    var expiry = $(self).data('expiry');
    var list = $(".data-holder[data-item="+item+"][data-expiry="+expiry+"]");
    var clients = {};
    $(list).each(function(i, row) {
      var qty = $(row).data("qty");
      var price = $(row).data('price');
	  var mulAmount=$(row).data('mulamount');
      var client = $(row).data("client");
      var transection = $(row).data('transection-type');

      qty = transection == "Sell" ? qty * (-1): qty;

      if(clients.hasOwnProperty(client)) {
        clients[client].landingCost += (qty * price);
        clients[client].totalQty += qty;
      } else {
        clients[client] = {
          'landingCost': (qty * price),
          'totalQty': qty
        }
      }
    }).promise().done(function(){
      var item = $(self).data('item');
      var expiry = $(self).data('expiry');
	  var mulamount=$(self).data('mulamount');
      var totalAmount = 0;
      for(clientId in clients) {
        var clientDetails = clients[clientId];
        var calculatedAmount = (clientDetails.totalQty * $(self).val()) - clientDetails.landingCost ;
        totalAmount += calculatedAmount;
        $(".calculation-holder[data-client="+clientId+"][data-item="+item+"][data-expiry="+expiry+"]")
          .text(Math.round(calculatedAmount*mulamount*100)/100)
          .removeClass("lossStyle")
          .removeClass("profitStyle")
          .addClass((calculatedAmount < 0 ?"lossStyle": "profitStyle"));
      }
      $(".total-amount-holder[data-item="+item+"][data-expiry="+expiry+"]")
        .text(totalAmount)
        .removeClass("lossStyle")
        .removeClass("profitStyle")
        .addClass((totalAmount < 0 ?"lossStyle": "profitStyle"));
      // total profit loss count
      var grandTotal = 0;
      $(".total-amount-holder").each(function(i, amtEle) {
        grandTotal += parseInt($(amtEle).text());
      }).promise().done(function() {
        $("#wholepl1")
        .text(grandTotal)
        .removeClass("lossStyle")
        .removeClass("profitStyle")
        .addClass((grandTotal < 0 ?"lossStyle": "profitStyle"));
      });
    });
    // OLD CODE COMMENTED BY HK
    // var rowids = $(this).closest('tr').attr('id');     
    // var expival = $("#"+rowids+"_exp").val();
    // var itemval = $("#"+rowids+"_item").val();
    // var totprice = 0;
    // $("#itemtable tr").each(function(){
    //  var rowid = $(this).attr('id'); 
    //  var rowcls = $(this).attr('class');       
    //  var expval = $("#"+rowid+"_exp").val();
    //  var ival = $("#"+rowid+"_item").val();
    //  if(expval == expival && itemval == ival){
    //    var buy_qty = $("#"+rowid+"_buy").val();
    //    var sell_qty = $("#"+rowid+"_sell").val();
    //    //var quanval = sell_qty-buy_qty;
    //    //$("#row_"+rowcls+"_quan").html(quanval);
    //    var buyprice = 0;               
    //    var clientIds = $("#"+rowid+"_client").val();
    //    $("."+clientIds+"_"+ival+"_"+expival+"_buyprice").each(function(){
    //      buyprice = parseInt(buyprice)+parseInt($(this).val());              
    //    });
    //    //var quanval = $("#row_"+rowcls+"_quan").html();     
    //    var priceval = $("#"+rowids+"_price").val();
    //    var price = (sell_qty*priceval)-buyprice;
    //    totprice = totprice+price;
        
    
    //    /*if(quanval < 0){
    //      $("#row_"+rowcls+"_quan1").html(quanval.replace('-', '')+" qty, Each one sell at "+priceval);
    //    } else {
    //      $("#row_"+rowcls+"_quan1").html(quanval+" qty buy at "+priceval);
    //    }*/
        
    //    $("#row_"+rowcls+"_price").html(price);
    //    $("#whole_"+ival+"_"+expival).html(totprice);
    //    $("#whole1_"+ival+"_"+expival).html(totprice);
    //    $("#wholeval_"+ival+"_"+expival).val(totprice);           
    //    var clientTotal = 0;
    //    $("."+clientIds+"_pl").each(function(){
        
    //    if($(this).html()!=''){
    //      clientTotal = parseInt(clientTotal) + parseInt($(this).html());
    //      $("#"+clientIds+"_itemtotal").html(clientTotal);
    //      }
    //    });
    //  }
                
    // });
    // var totalprice = 0;
    //    $(".wholeprice").each(function(){
    //      var wholeval = $(this).val();
    //      if(wholeval!=''){
    //        totalprice = parseInt(totalprice)+parseInt(wholeval); 
    //      }
    //    });
    // $("#wholepl").html(totalprice);
    // $("#wholepl1").html(totalprice);
  });

  var calculations = eval("("+window.sessionStorage.getItem('calculations')+")");

  if(calculations != undefined) {
    for(i in calculations) {
      var ele = calculations[i];
      var htmlEle = $(".price[data-item="+ele.item+"][data-expiry="+ele.expiry+"]");
      $(htmlEle).val(ele.value);
      $(htmlEle).change();
    }
  }

});

</script>
{/literal}


</HEAD>
<BODY>
	<a href="./index.php">Home</a> &nbsp;<a href="./tradeAdd.php">Add Trade</a> &nbsp;<A href='addTrade.php?exchange=CX'>New Add Trade</A>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
 <table cellPadding="5" cellSpacing="5" border="1" style="margin-bottom:20px;">
    <tr>
      <th>Item</th>
      <th>Expiry</th>
      <th>Current Price</th>
    </tr>
    {foreach from=$expiryDetails item=eDetails}
    <tr id="val_{$eDetails.id}">
      <td>{$eDetails.itemId}
        <input type="hidden" id="val_{$eDetails.id}_item" value="{$eDetails.itemId}"></td>
      <td>{$eDetails.expiryDate}
        <input type="hidden" id="val_{$eDetails.id}_exp" value="{$eDetails.expiryDate}"></td>
      <td><input
    type="text"
    class="price"
    id="val_{$eDetails.id}_price"
    data-expiry="{$eDetails.expiryDate}"
    data-mulamount="{$eDetails.mulAmount}"
    data-item="{$eDetails.itemId}"
  ></td>
    </tr>
    {/foreach}
    <tr>
      <td colspan="3" style="text-align: right"><button id="storeLocally">Save</button></td>
    </tr>
  </table>
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
    <SELECT name="displayBuySell" onChange="document.form1.submit();">
    {html_options selected="$displayBuySellSelected" values="$displayBuySellValues" output="$displayBuySellOptions"}
    </SELECT>
  </TD>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date1day}&fromDateMonth={$date1month}&fromDateYear={$date1year}&toDateDay={$date1day}&toDateMonth={$date1month}&toDateYear={$date1year}&submitBtn=Display...">{$date1day}</A>&nbsp;|&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date2day}&fromDateMonth={$date2month}&fromDateYear={$date2year}&toDateDay={$date2day}&toDateMonth={$date2month}&toDateYear={$date2year}&submitBtn=Display...">{$date2day}</A>&nbsp;|&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date3day}&fromDateMonth={$date3month}&fromDateYear={$date3year}&toDateDay={$date3day}&toDateMonth={$date3month}&toDateYear={$date3year}&submitBtn=Display...">{$date3day}</A>&nbsp;|&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date4day}&fromDateMonth={$date4month}&fromDateYear={$date4year}&toDateDay={$date4day}&toDateMonth={$date4month}&toDateYear={$date4year}&submitBtn=Display...">{$date4day}</A>&nbsp;|&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date5day}&fromDateMonth={$date5month}&fromDateYear={$date5year}&toDateDay={$date5day}&toDateMonth={$date5month}&toDateYear={$date5year}&submitBtn=Display...">{$date5day}</A>&nbsp;|&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date6day}&fromDateMonth={$date6month}&fromDateYear={$date6year}&toDateDay={$date6day}&toDateMonth={$date6month}&toDateYear={$date6year}&submitBtn=Display...">{$date6day}</A>&nbsp;|&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}&fromDateDay={$date7day}&fromDateMonth={$date7month}&fromDateYear={$date7year}&toDateDay={$date7day}&toDateMonth={$date7month}&toDateYear={$date7year}&submitBtn=Display...">{$date7day}</A>&nbsp;|&nbsp;&nbsp;
    
    <A href="selectDtSession.php">Date range</A> : {$fromDate} To : {$toDate}
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</FORM>
</TABLE>
<FORM name="form2" method="post" action="{$PHP_SELF}">
  <INPUT type="hidden" name="display" value="{$display}">
  <INPUT type="hidden" name="itemIdChanged" value="0">
	<input type="hidden" name="clientId" value="{$clientIdSelected}">
	<input type="hidden" name="itemId" value="{$itemIdSelected}">
	<input type="hidden" name="expiryDate" value="{$expiryDateSelected}">
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6"><input type="submit" name="confirmBtn1" value="! Done !"></TD>
</TR>
<TR>
  <TD align="center">BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">NetProfitLoss</TD>
  <TD align="center">Vendor</TD>
  <TD align="center">Delete</TD>
  {if $display == "detailed"} 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  {/if}
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].clientId != $trades[sec1].prevClientId or $trades[sec1].itemId != $trades[sec1].prevItemId or $trades[sec1].expiryDate != $trades[sec1].prevExpiryDate}
  <TR>
    <TD colspan="5"><U>{$trades[sec1].clientId} : {$trades[sec1].clientName}</U></TD>
    <TD colspan="6" align="center">{$trades[sec1].itemIdExpiry}</TD>
  </TR>
{/if}
<TR style="color:{$trades[sec1].fontColor}" 
class="data-holder"
    data-client="{$trades[sec1].clientId}"
    data-item="{$trades[sec1].itemId}"
    data-expiry="{$trades[sec1].expiryDate}"
    data-mulamount="{$trades[sec1].mulAmount}"
    data-transection-type="{$trades[sec1].buySell}"
    data-qty="{if ($trades[sec1].buySell == 'Buy')}{$trades[sec1].buyQty}{else}{$trades[sec1].sellQty}{/if}"
    data-price="{if ($trades[sec1].buySell == 'Buy') }{$trades[sec1].price}{else}{$trades[sec1].sellPrice}{/if}">

  <TD align="center">{$trades[sec1].buySell}</TD>
  <TD align="right">{$trades[sec1].buyQty}</TD>
  <TD align="right">
  {$trades[sec1].price}
  {if $trades[sec1].price != "&nbsp;"}
    <input type="checkbox" name="confirmed[{$trades[sec1].tradeId}]" value="{$trades[sec1].tradeId}" {if $trades[sec1].confirmed == "1"} CHECKED {/if}>
  {/if}
  </TD>
  <TD align="right">{$trades[sec1].sellQty}</TD>
  <TD align="right">
  	 {$trades[sec1].sellPrice}
    {if $trades[sec1].sellPrice != "&nbsp;"}
    <input type="checkbox" name="confirmed[{$trades[sec1].tradeId}]" value="{$trades[sec1].tradeId}" {if $trades[sec1].confirmed == "1"} CHECKED {/if}>
    {/if}
  </TD>
  <TD align="center" NOWRAP>{$trades[sec1].tradeDate}
    {if $display == "detailed"} {$trades[sec1].tradeTime} {/if}
  </TD>
  <TD>{$trades[sec1].standing}</TD>
  <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
  <TD>&nbsp;</TD>
  <TD>{$trades[sec1].vendor}</TD>
  <TD><!--<A onclick="return confirm('Are you sure?');" href="deleteTxt.php?tradeId={$trades[sec1].tradeId}">Delete</A></TD>-->
    <A onClick="return confirm('Are you sure?');" href="deleteTxt.php?tradeId={$trades[sec1].tradeId}">
    {if $trades[sec1].standing != "Close"}
      Delete
    {else}
      Delete Stand
    {/if}
    </A>
    &nbsp;
    <a href="addTrade.php?exchange={$currentExchange}&amp;tradeId={$trades[sec1].tradeId}&amp;goTo={$goTo}">Edit</a>
  </TD>
  {if $display == "detailed"} 
    <TD align="center">{$trades[sec1].userRemarks}</TD>
    <TD align="center">{$trades[sec1].ownClient}</TD>
    <TD align="center" NOWRAP>{$trades[sec1].tradeRefNo}</TD>
  {/if}
</TR>
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD align="right" NOWRAP>
      Net : {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
    </TD>
    <TD align="right">{$trades[sec1].totBuyQty}</TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD align="right">{$trades[sec1].totSellQty}</TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="3" align="right" NOWRAP>
      {if $trades[sec1].profitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].profitLoss}</FONT>
      Brok       : {$trades[sec1].oneSideBrok}</TD>
    <TD align="right" NOWRAP>
      {if $trades[sec1].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].netProfitLoss}</FONT>
    </TD>
    <TD colspan="2" align="right">InRs : {$trades[sec1].netProfitLossInRs}</TD>
  {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
  {/if}
  </TR>
  
   <TR id="{$trades[sec1].clientId}_{$trades[sec1].tradeId}" > 
    <!--td colspan="3">Quantity: <span id="row_{$trades[sec1].clientId}_{$trades[sec1].tradeId}_quan"></span></td-->
    <td colspan="4">Profit/Loss: <span 
      id="row_{$trades[sec1].clientId}_{$trades[sec1].tradeId}_price"
      class="{$trades[sec1].clientId}_pl calculation-holder"
      data-client="{$trades[sec1].clientId}"
      data-item="{$trades[sec1].itemId}"
      data-expiry="{$trades[sec1].expiryDate}"> </span></td>
    <!--td colspan="5">Quan With Price: <span id="row_{$trades[sec1].clientId}_{$trades[sec1].tradeId}_quan1"></span></td--> 
  </TR>
  
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="5" align="right">
      {$trades[sec1].clientId} : {$trades[sec1].clientName}
      : Total : 
    </TD>
    <TD colspan="3" align="right"><U>
      {if $trades[sec1].clientTotProfitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].clientTotProfitLoss}</FONT></U>
      Brok       : {$trades[sec1].clientTotBrok}</U>
    </TD>
    <TD align="right"><U>
      {if $trades[sec1].clientTotNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].clientTotNetProfitLoss}</FONT></U></TD>
    <TD colspan="2" align="right">InRs : {$trades[sec1].clientTotNetProfitLossInRs}</TD>
  </TR>
  {/if}
{/if}
{/section}
<TR>
	<TD colspan="5">&nbsp;</TD>
	<TD colspan="6"><input type="submit" name="confirmBtn2" value="! Done !"></TD>
</TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].profitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].profitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].netProfitLoss}</FONT></TD>
</TR>
{/section}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>
    {if $wholeProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeProfitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeNetProfitLoss}</FONT></TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
