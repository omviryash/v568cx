<?php
include "etc/om_config.inc";

if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=standing");
}
else
{
  $day = date("w");
  
  if($day == 0)
    $oldDay =  6;
  elseif($day == 1)
    $oldDay =  7;
  else
    $oldDay =  $day - 1;
    
  $standingDtCurrent         = date("Y-m-d");
  $standingDtCurrentDDMMYYYY = date("d-m-Y");
  $standingDtCurrentWeekDay  = date("l");
  
  if($day <= 4) // <=4 means thu/wed/tue/mon/sun
    $dayPlus = 1;
  else  // > 4 means fri/sat/sun
    $dayPlus = 7-$oldDay;
  $standingDtNext         = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")+$dayPlus, date("Y")));
  $standingDtNextDDMMYYYY = date("d-m-Y", mktime(0, 0, 0, date("m")  , date("d")+$dayPlus, date("Y")));;
  $standingDtNextWeekDay  = date("l", mktime(0, 0, 0, date("m")  , date("d")+$dayPlus, date("Y")));;
///////////////////////////////////////
  $itemRows = '';

  $selectStandingQuery = "SELECT * FROM expiry ORDER BY itemId";
  $resultStandingQuery = mysql_query($selectStandingQuery );
  $itemIdArray = array();
  $expiryDateArray = array();
  $itemIdExpiryDateNameArray = array();
  $i = 0;
  while($standingRows = mysql_fetch_array($resultStandingQuery))
  {
    $itemIdArray[$i]               = $standingRows['itemId'];
    $expiryDateArray[$i]           = $standingRows['expiryDate'];
    $itemIdExpiryDateNameArray[$i] = $standingRows['itemId'].$standingRows['expiryDate'];
    $i++;
  }
///////////////////////////////////////
  $smarty = new SmartyWWW();
  $smarty->assign("standingDtCurrent", $standingDtCurrent);
  $smarty->assign("standingDtNext",    $standingDtNext);
  $smarty->assign("standingDtCurrentDDMMYYYY", $standingDtCurrentDDMMYYYY);
  $smarty->assign("standingDtCurrentWeekDay" , $standingDtCurrentWeekDay);
  $smarty->assign("standingDtNextDDMMYYYY"   , $standingDtNextDDMMYYYY);
  $smarty->assign("standingDtNextWeekDay"    , $standingDtNextWeekDay);
  $smarty->assign("itemIdArray"    , $itemIdArray);
  $smarty->assign("expiryDateArray", $expiryDateArray);
  $smarty->assign("itemIdExpiryDateNameArray",    $itemIdExpiryDateNameArray);
  $smarty->display("standing.tpl");
}
?>